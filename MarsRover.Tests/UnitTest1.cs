using System;
using Xunit;
using FluentAssertions;
using GeorgeCloney;
using System.Collections.Generic;
using MarsRover.Core;
using System.Linq;

namespace MarsRover.Tests
{
    public class MarsRoverTests
    {
        [Fact]
        public void RoverStartsAtCordinatesX0_Y0()
        {
            // -- arrange
            var rover = new Rover();

            // -- act
            var initialCordinates = rover.Cordinates;

            // -- assert
            initialCordinates.Should().BeEquivalentTo( new Cordinates(), "because rover starts at position 0,0");
        }

        // test that we are heading north by default

        [Fact]
        public void GivenEmptyCommandItStaysInPlace()
        {
            // -- arrange
            var rover = new Rover();
            var initialCordinates = rover.Cordinates;

            // -- act
            rover.DoInsts("");

            // -- assert
            rover.Cordinates.Should().BeEquivalentTo(initialCordinates, "because we gave the rover empty instruction and so it should not move");
        }

        [Fact]
        public void Given_L_ItRotatesTo_West()
        {
            // -- arrange
            var rover = new Rover();

            // -- act
            rover.DoInsts("L");

            // -- assert
            rover.Direction.Should().BeEquivalentTo(Direction.West, o => o.ComparingEnumsByName());
        }

        [Fact]
        public void Given_R_ItRotatesTo_East()
        {
            // -- arrange
            var rover = new Rover();

            // -- act
            rover.DoInsts("R");

            // -- assert
            rover.Direction.Should().BeEquivalentTo(Direction.East, o => o.ComparingEnumsByName());
        }

        [Fact]
        public void Given_M_ItDrivesToX0_Y1()
        {
            // -- arrange
            var rover = new Rover();
            var expectedCordinates = new Cordinates(0, 1);

            // -- act
            rover.DoInsts("M");

            // -- assert
            rover.Cordinates.Should().BeEquivalentTo(expectedCordinates, "because we were heading to North and we moved by one field forward");
        }

        [Fact]
        public void StartingFromX1Y1_AndHeadingTo_E_Given_M_ItDrivesToX2_Y1()
        {
            // -- arrange
            var rover = new Rover(1, 1, Direction.East);
            var expectedCordinates = new Cordinates(2, 1);

            // -- act
            rover.DoInsts("M");

            // -- assert
            rover.Cordinates.Should().BeEquivalentTo(expectedCordinates, "because we were heading to East and we moved by one field forward");
        }

        [Fact]
        public void StartingFromX1Y1_AndHeadingTo_W_Given_M_ItDrivesTo_X0Y1()
        {
            // -- arrange
            var rover = new Rover(1, 1, Direction.West);
            var expectedCordinates = new Cordinates(0, 1);

            // -- act
            rover.DoInsts("M");

            // -- assert
            rover.Cordinates.Should().BeEquivalentTo(expectedCordinates);
        }

        [Theory]
        [InlineData(Direction.North, Direction.West)]
        [InlineData(Direction.West, Direction.South)]
        [InlineData(Direction.South, Direction.East)]
        [InlineData(Direction.East, Direction.North)]
        public void ItCanTurnToLeft(Direction initial, Direction expected)
        {
            // -- arrange
            var rover = new Rover(0, 0, initial);

            // -- act
            rover.DoInsts("L");

            // -- assert
            rover.Direction.Should().Be(expected);
        }

        [Theory]
        [InlineData(Direction.North, Direction.East) ]
        [InlineData(Direction.East,  Direction.South)]
        [InlineData(Direction.South, Direction.West) ]
        [InlineData(Direction.West,  Direction.North)]
        public void ItCanTurnToRight(Direction initial, Direction expected)
        {
            // -- arrange
            var rover = new Rover(0, 0, initial);

            // -- act
            rover.DoInsts("R");

            // -- assert
            rover.Direction.Should().Be(expected);
        }

        [Fact]
        public void GivenACommandWithTwoInstructionsItDoesThemBoth()
        {
            // -- arrange 
            var rover = new Rover(0, 0, Direction.North);

            // -- act 
            rover.DoInsts("LLM");

            // -- assert
            rover.Cordinates.Should().BeEquivalentTo(new Cordinates(0, -1));
        }


        [Fact]
        public void GivenAnUnknownInstructionItThrowsAnInvalidOperationException()
        {
            // -- arrange 
            var rover = new Rover();

            // -- act & assert
            rover.Invoking(r => r.DoInsts("X")).Should().Throw<InvalidOperationException>();
        }

        /// TESTS FOR GETER OF INSTRUCTIONS
        /// 
        [Theory]
        [InlineData(0,  0, Direction.North, 2, -3, Direction.West)]
        [InlineData(0,  0, Direction.West,  2, -3, Direction.West)]
        [InlineData(2, -3, Direction.West,  2, -3, Direction.West)]
        [InlineData(-2,  9, Direction.North,  2, -3, Direction.West)]
        [InlineData(1,  0, Direction.North,  0, 0, Direction.West)]
        [InlineData(-6, 9, Direction.East, -6, 4, Direction.East)]
        [InlineData(0, 9, Direction.North, 8, -1, Direction.North)]
        [InlineData(4, 9, Direction.East, 5, 1, Direction.West)]
        [InlineData(1, -2, Direction.North, 6, -1, Direction.North)]
        [InlineData(-7, 6, Direction.North, 4, -4, Direction.West)]
        [InlineData(-8, 4, Direction.West, 9, -6, Direction.East)]
        [InlineData(-9, 1, Direction.West, 4, 0, Direction.West)]
        [InlineData(7, 5, Direction.East, -5, -7, Direction.North)]
        [InlineData(5, -4, Direction.North, 9, 3, Direction.North)]
        [InlineData(5, -2, Direction.West, 2, 4, Direction.West)]
        [InlineData(4, 4, Direction.North, 5, 1, Direction.North)]
        [InlineData(-5, -6, Direction.East, 7, -9, Direction.East)]
        [InlineData(-10, -3, Direction.North, -10, -2, Direction.North)]
        [InlineData(-6, 4, Direction.North, -5, -10, Direction.East)]
        [InlineData(-7, -5, Direction.North, -8, -2, Direction.West)]
        [InlineData(-5, 1, Direction.West, 0, 5, Direction.North)]
        [InlineData(-1, -3, Direction.North, -3, -8, Direction.North)]
        [InlineData(-3, 2, Direction.East, -1, 5, Direction.North)]
        [InlineData(8, 0, Direction.West, -7, 9, Direction.North)]
        [InlineData(9, 2, Direction.West, 3, -5, Direction.East)]
        [InlineData(-5, -10, Direction.West, 3, -5, Direction.East)]
        [InlineData(9, -7, Direction.East, -4, -6, Direction.East)]
        [InlineData(2, 0, Direction.East, -2, -2, Direction.East)]
        [InlineData(-10, -3, Direction.North, -6, 4, Direction.East)]
        [InlineData(-8, 1, Direction.East, -2, 3, Direction.North)]
        [InlineData(-2, 5, Direction.East, 6, 1, Direction.East)]
        [InlineData(-2, 3, Direction.North, -10, -3, Direction.East)]
        [InlineData(-9, 4, Direction.West, -9, -7, Direction.North)]
        [InlineData(7, 7, Direction.East, -3, -8, Direction.East)]
        [InlineData(-5, 7, Direction.East, 5, 1, Direction.West)]
        [InlineData(-5, -10, Direction.North, -4, -6, Direction.North)]
        [InlineData(2, -7, Direction.East, 1, -1, Direction.East)]
        [InlineData(8, 1, Direction.West, 4, 5, Direction.West)]
        [InlineData(9, 6, Direction.East, -3, 3, Direction.West)]
        [InlineData(-10, -8, Direction.North, 5, -10, Direction.West)]
        [InlineData(3, 3, Direction.East, 0, 7, Direction.East)]
        [InlineData(-8, 9, Direction.North, -4, -2, Direction.West)]
        [InlineData(-8, 7, Direction.West, 6, -5, Direction.West)]
        [InlineData(9, 7, Direction.East, -7, -8, Direction.East)]
        [InlineData(-3, 2, Direction.North, -2, 9, Direction.North)]
        [InlineData(-3, 0, Direction.North, 8, -5, Direction.East)]
        [InlineData(2, 6, Direction.East, -2, -4, Direction.East)]
        [InlineData(1, -8, Direction.East, 7, 0, Direction.East)]
        [InlineData(3, 8, Direction.North, -10, -10, Direction.East)]
        [InlineData(3, 4, Direction.North, 1, -2, Direction.North)]
        [InlineData(9, 2, Direction.West, -1, -6, Direction.North)]
        [InlineData(1, 7, Direction.East, -6, -1, Direction.West)]
        [InlineData(9, 2, Direction.North, -1, -7, Direction.West)]
        [InlineData(-5, 9, Direction.North, -7, -6, Direction.East)]
        [InlineData(-9, 1, Direction.North, 7, 2, Direction.West)]
        [InlineData(-10, -5, Direction.West, 7, 6, Direction.East)]
        [InlineData(-4, 4, Direction.North, 7, -3, Direction.North)]
        [InlineData(8, 9, Direction.West, 6, 3, Direction.West)]
        [InlineData(4, 7, Direction.North, 9, 8, Direction.West)]
        [InlineData(8, -9, Direction.West, 5, 5, Direction.West)]
        [InlineData(8, 2, Direction.East, 1, 0, Direction.North)]
        [InlineData(-4, -8, Direction.West, 0, -6, Direction.West)]
        [InlineData(-4, 0, Direction.North, 5, -2, Direction.East)]
        [InlineData(9, 9, Direction.West, 5, -3, Direction.East)]
        [InlineData(-3, 3, Direction.West, 2, -10, Direction.North)]
        [InlineData(4, 6, Direction.West, 3, -6, Direction.East)]
        [InlineData(7, 4, Direction.East, -7, -6, Direction.North)]
        [InlineData(-6, 6, Direction.West, 3, 1, Direction.North)]
        [InlineData(9, -4, Direction.West, -2, -7, Direction.East)]
        [InlineData(5, 8, Direction.North, -7, -4, Direction.East)]
        [InlineData(5, -2, Direction.West, -2, -6, Direction.West)]
        [InlineData(5, 6, Direction.West, 0, -1, Direction.West)]
        [InlineData(-1, -5, Direction.East, 7, 0, Direction.East)]
        [InlineData(7, -3, Direction.North, 2, 6, Direction.West)]
        [InlineData(-8, 1, Direction.East, -7, 1, Direction.West)]
        [InlineData(-9, 1, Direction.North, -6, 2, Direction.West)]
        [InlineData(4, -9, Direction.West, -4, -10, Direction.East)]
        [InlineData(9, -10, Direction.East, 8, -2, Direction.East)]
        [InlineData(-5, 7, Direction.North, 8, -10, Direction.East)]
        [InlineData(-4, -8, Direction.East, -5, 3, Direction.West)]
        [InlineData(8, 4, Direction.West, 1, -7, Direction.East)]
        [InlineData(-3, -4, Direction.West, 7, -10, Direction.East)]
        [InlineData(-7, -8, Direction.East, -3, -3, Direction.West)]
        [InlineData(1, 5, Direction.North, 9, 4, Direction.West)]
        [InlineData(-6, 1, Direction.West, -8, -1, Direction.North)]
        [InlineData(-3, 5, Direction.West, -1, -10, Direction.West)]
        [InlineData(2, -1, Direction.West, -8, -4, Direction.West)]
        [InlineData(-4, 2, Direction.West, 7, 4, Direction.East)]
        [InlineData(-2, -8, Direction.North, 1, 5, Direction.West)]
        [InlineData(4, -4, Direction.West, -1, -8, Direction.West)]
        [InlineData(-6, 3, Direction.West, -1, 2, Direction.East)]
        [InlineData(-8, -7, Direction.East, 4, -8, Direction.East)]
        [InlineData(7, -9, Direction.North, -3, -10, Direction.East)]
        [InlineData(8, -1, Direction.East, 9, -8, Direction.East)]
        [InlineData(-9, 3, Direction.East, 7, 3, Direction.North)]
        [InlineData(8, 5, Direction.West, 5, -9, Direction.East)]
        [InlineData(-1, 7, Direction.West, -2, -3, Direction.East)]
        [InlineData(-8, 3, Direction.East, 2, 9, Direction.North)]
        [InlineData(-1, -8, Direction.West, -1, 5, Direction.North)]
        [InlineData(9, -4, Direction.West, 5, 8, Direction.North)]
        [InlineData(-7, -6, Direction.North, 5, 2, Direction.North)]
        [InlineData(8, 8, Direction.East, -10, 4, Direction.North)]
        [InlineData(5, 6, Direction.North, -10, 8, Direction.North)]
        [InlineData(8, -10, Direction.North, 7, 5, Direction.East)]
        [InlineData(7, -10, Direction.East, 12, -16, Direction.East)]
        [InlineData(-22, -3, Direction.West, -24, 10, Direction.West)]
        [InlineData(11, 13, Direction.North, 9, 8, Direction.West)]
        [InlineData(-19, 6, Direction.North, 4, -11, Direction.East)]
        [InlineData(-19, 6, Direction.North, -14, 13, Direction.North)]
        [InlineData(-6, -21, Direction.East, -4, 3, Direction.East)]
        [InlineData(-2, -9, Direction.West, -24, -21, Direction.East)]
        [InlineData(8, -8, Direction.North, -20, -4, Direction.West)]
        [InlineData(-25, 2, Direction.West, 0, 5, Direction.West)]
        [InlineData(7, -18, Direction.West, 2, -21, Direction.West)]
        [InlineData(-5, 5, Direction.West, -2, -16, Direction.West)]
        [InlineData(-14, 9, Direction.East, 12, -6, Direction.East)]
        [InlineData(11, -10, Direction.East, 10, -15, Direction.North)]
        [InlineData(11, -9, Direction.West, -22, 6, Direction.West)]
        [InlineData(-22, -11, Direction.West, 12, -12, Direction.North)]
        [InlineData(7, -21, Direction.North, 1, -10, Direction.North)]
        [InlineData(-1, -20, Direction.North, 0, -6, Direction.East)]
        [InlineData(3, -5, Direction.East, -17, -1, Direction.East)]
        [InlineData(-4, -17, Direction.West, -9, -15, Direction.North)]
        [InlineData(-16, 14, Direction.East, 1, -24, Direction.East)]
        [InlineData(12, 9, Direction.East, -25, -10, Direction.North)]
        [InlineData(-7, -1, Direction.East, 1, 0, Direction.East)]
        [InlineData(-20, -19, Direction.West, 14, -11, Direction.North)]
        [InlineData(-10, -7, Direction.East, -5, 11, Direction.West)]
        [InlineData(-18, -23, Direction.North, -6, -7, Direction.East)]
        [InlineData(-13, -6, Direction.East, 5, 0, Direction.West)]
        [InlineData(-6, 4, Direction.East, -12, 14, Direction.West)]
        [InlineData(-22, -20, Direction.West, -22, -25, Direction.North)]
        [InlineData(-24, -8, Direction.West, 13, -12, Direction.North)]
        [InlineData(-16, -7, Direction.East, -2, -2, Direction.West)]
        [InlineData(-11, -6, Direction.West, -24, 7, Direction.North)]
        [InlineData(-13, -12, Direction.East, -16, -10, Direction.West)]
        [InlineData(-6, 3, Direction.East, -4, -14, Direction.West)]
        [InlineData(14, -21, Direction.North, 4, 13, Direction.North)]
        [InlineData(-17, -16, Direction.West, -16, 13, Direction.North)]
        [InlineData(-20, -15, Direction.West, 1, 2, Direction.West)]
        [InlineData(1, -11, Direction.West, 4, 4, Direction.East)]
        [InlineData(-14, -24, Direction.East, -22, -25, Direction.East)]
        [InlineData(-13, 7, Direction.West, -22, 11, Direction.North)]
        [InlineData(-2, -12, Direction.East, -8, -9, Direction.West)]
        [InlineData(5, -10, Direction.West, -17, 1, Direction.East)]
        [InlineData(13, 11, Direction.West, -21, -13, Direction.West)]
        [InlineData(-6, -13, Direction.East, -3, -11, Direction.West)]
        [InlineData(-9, 14, Direction.West, 10, -3, Direction.North)]
        [InlineData(-13, -8, Direction.West, -16, -2, Direction.West)]
        [InlineData(5, -24, Direction.North, 7, 8, Direction.North)]
        [InlineData(-8, -8, Direction.North, -7, 2, Direction.North)]
        [InlineData(14, 6, Direction.East, -22, 3, Direction.West)]
        [InlineData(-14, 5, Direction.West, 9, -25, Direction.North)]
        [InlineData(-24, -23, Direction.North, -24, -18, Direction.West)]
        [InlineData(-16, -14, Direction.North, -15, -20, Direction.North)]
        [InlineData(-23, -19, Direction.West, 12, 0, Direction.East)]
        [InlineData(13, -20, Direction.East, -25, -11, Direction.East)]
        [InlineData(-17, 0, Direction.North, -3, -22, Direction.North)]
        [InlineData(14, 14, Direction.North, -23, 8, Direction.East)]
        [InlineData(-7, 13, Direction.North, -7, 4, Direction.West)]
        [InlineData(-5, 14, Direction.East, 3, -5, Direction.North)]
        [InlineData(-4, -22, Direction.North, -23, -12, Direction.West)]
        [InlineData(-25, 2, Direction.North, 3, 10, Direction.North)]
        [InlineData(-9, 11, Direction.West, -22, -3, Direction.East)]
        [InlineData(-20, -2, Direction.West, -19, -23, Direction.West)]
        [InlineData(8, -24, Direction.West, -22, -25, Direction.West)]
        [InlineData(14, -23, Direction.North, -2, -7, Direction.East)]
        [InlineData(-7, 2, Direction.West, 14, -20, Direction.West)]
        [InlineData(7, -12, Direction.East, 1, 6, Direction.East)]
        [InlineData(8, -16, Direction.North, -25, -11, Direction.West)]
        [InlineData(-1, -3, Direction.West, 12, -3, Direction.West)]
        [InlineData(4, 5, Direction.North, 8, -22, Direction.West)]
        [InlineData(8, -20, Direction.North, 7, 6, Direction.West)]
        [InlineData(7, -19, Direction.North, 2, -12, Direction.East)]
        [InlineData(5, -25, Direction.North, -24, 5, Direction.North)]
        [InlineData(-21, -5, Direction.East, 1, -18, Direction.West)]
        [InlineData(-10, 8, Direction.East, 1, -16, Direction.East)]
        [InlineData(-3, 4, Direction.East, -5, -13, Direction.North)]
        [InlineData(-5, 7, Direction.North, 5, 14, Direction.East)]
        [InlineData(13, -9, Direction.West, 9, -13, Direction.North)]
        [InlineData(-6, -19, Direction.North, -13, 13, Direction.East)]
        [InlineData(12, 4, Direction.West, -10, -11, Direction.West)]
        [InlineData(-3, 3, Direction.West, -12, 8, Direction.East)]
        [InlineData(-23, 6, Direction.East, 6, -23, Direction.North)]
        [InlineData(-18, 14, Direction.East, -10, 14, Direction.North)]
        [InlineData(-23, -1, Direction.West, -17, 9, Direction.West)]
        [InlineData(8, 9, Direction.East, -5, -17, Direction.West)]
        [InlineData(8, 0, Direction.West, -11, -5, Direction.West)]
        [InlineData(4, 11, Direction.West, 11, -6, Direction.East)]
        [InlineData(-22, -11, Direction.West, -21, -5, Direction.East)]
        [InlineData(-24, -16, Direction.North, -11, -1, Direction.East)]
        [InlineData(4, 4, Direction.West, 3, 9, Direction.West)]
        [InlineData(-13, -7, Direction.West, 6, -16, Direction.North)]
        [InlineData(-19, -9, Direction.East, 11, -16, Direction.East)]
        [InlineData(0, -20, Direction.West, 3, 9, Direction.East)]
        [InlineData(-17, -8, Direction.West, -3, 13, Direction.West)]
        [InlineData(-12, -17, Direction.North, -5, -13, Direction.East)]
        [InlineData(-19, -14, Direction.North, -22, 6, Direction.East)]
        [InlineData(0, -17, Direction.West, -10, -21, Direction.West)]
        [InlineData(13, 5, Direction.North, -17, -19, Direction.West)]
        [InlineData(8, -9, Direction.North, 3, -19, Direction.West)]
        [InlineData(-14, -10, Direction.North, -19, -9, Direction.East)]
        [InlineData(-20, -18, Direction.West, 1, 6, Direction.East)]
        [InlineData(12, -2, Direction.West, 11, -21, Direction.North)]
        [InlineData(-23, 2, Direction.East, -22, -18, Direction.West)]
        [InlineData(10, -14, Direction.South, -7, -6, Direction.South)]
        [InlineData(-15, -8, Direction.South, -20, -22, Direction.North)]
        [InlineData(-6, 12, Direction.West, -2, 6, Direction.West)]
        [InlineData(6, 6, Direction.North, -14, 8, Direction.East)]
        [InlineData(-1, -4, Direction.North, 12, -25, Direction.North)]
        [InlineData(12, -15, Direction.East, -16, -8, Direction.East)]
        [InlineData(-20, -24, Direction.East, 12, -12, Direction.North)]
        [InlineData(1, -13, Direction.West, -12, -20, Direction.South)]
        [InlineData(-2, -13, Direction.West, 10, -13, Direction.North)]
        [InlineData(-1, -10, Direction.South, 1, 1, Direction.North)]
        [InlineData(3, -18, Direction.West, -1, -5, Direction.West)]
        [InlineData(2, -5, Direction.South, -21, -24, Direction.West)]
        [InlineData(7, 4, Direction.East, -11, -8, Direction.South)]
        [InlineData(-11, -1, Direction.West, -5, -12, Direction.North)]
        [InlineData(6, -21, Direction.North, 0, 8, Direction.North)]
        [InlineData(5, -6, Direction.South, -10, -13, Direction.East)]
        [InlineData(-13, 9, Direction.North, -19, 3, Direction.East)]
        [InlineData(-2, -18, Direction.West, -12, -14, Direction.East)]
        [InlineData(-16, 13, Direction.East, -15, -11, Direction.West)]
        [InlineData(-8, 6, Direction.West, 11, 9, Direction.North)]
        [InlineData(-4, 2, Direction.South, -18, -17, Direction.West)]
        [InlineData(12, 11, Direction.East, -1, -9, Direction.North)]
        [InlineData(-10, -5, Direction.South, -3, -18, Direction.West)]
        [InlineData(-15, -3, Direction.West, -16, 0, Direction.North)]
        [InlineData(-7, -15, Direction.South, -1, 13, Direction.North)]
        [InlineData(-12, 14, Direction.East, -17, -24, Direction.North)]
        [InlineData(-8, -12, Direction.East, -18, 0, Direction.West)]
        [InlineData(12, 14, Direction.East, -14, 7, Direction.West)]
        [InlineData(-14, -2, Direction.North, -23, -5, Direction.East)]
        [InlineData(-25, 11, Direction.North, 11, 10, Direction.South)]
        [InlineData(-23, 0, Direction.North, -25, -14, Direction.West)]
        [InlineData(14, 7, Direction.North, -12, 10, Direction.West)]
        [InlineData(-7, 1, Direction.North, 0, -5, Direction.South)]
        [InlineData(-2, 10, Direction.South, -6, -7, Direction.West)]
        [InlineData(-25, 4, Direction.East, 11, 2, Direction.North)]
        [InlineData(-5, -25, Direction.East, -6, 13, Direction.West)]
        [InlineData(-14, -5, Direction.West, -14, -3, Direction.South)]
        [InlineData(5, 14, Direction.North, -13, -12, Direction.North)]
        [InlineData(2, -8, Direction.West, 8, -5, Direction.South)]
        [InlineData(-10, -11, Direction.North, -21, 13, Direction.South)]
        [InlineData(-13, -2, Direction.South, 5, -6, Direction.East)]
        [InlineData(8, -23, Direction.North, -17, -14, Direction.South)]
        [InlineData(-1, 9, Direction.East, -10, 0, Direction.South)]
        [InlineData(-23, -4, Direction.West, 9, -13, Direction.East)]
        [InlineData(-7, -19, Direction.East, -1, -14, Direction.West)]
        [InlineData(-23, -15, Direction.North, 12, 10, Direction.South)]
        [InlineData(-14, 3, Direction.North, -23, -11, Direction.West)]
        [InlineData(13, -24, Direction.North, 10, -14, Direction.South)]
        [InlineData(12, -18, Direction.East, -15, 10, Direction.South)]
        [InlineData(-11, -7, Direction.West, -22, 11, Direction.East)]
        [InlineData(14, -24, Direction.West, 3, -10, Direction.West)]
        [InlineData(13, -4, Direction.South, -4, 6, Direction.North)]
        [InlineData(-12, -12, Direction.East, -8, 14, Direction.North)]
        [InlineData(11, 9, Direction.North, -19, -17, Direction.South)]
        [InlineData(-14, 12, Direction.North, 9, -5, Direction.North)]
        [InlineData(-8, -5, Direction.North, 13, -20, Direction.North)]
        [InlineData(-9, -5, Direction.North, 6, -10, Direction.North)]
        [InlineData(12, 14, Direction.South, 12, -15, Direction.East)]
        [InlineData(-4, -25, Direction.North, -16, -9, Direction.West)]
        [InlineData(-7, -19, Direction.North, -7, -1, Direction.North)]
        [InlineData(-21, -21, Direction.North, -19, 4, Direction.West)]
        [InlineData(-17, 1, Direction.South, 7, -15, Direction.East)]
        [InlineData(6, -18, Direction.East, -3, -4, Direction.West)]
        [InlineData(5, 3, Direction.East, -24, 7, Direction.West)]
        [InlineData(-11, 14, Direction.East, 7, -23, Direction.North)]
        [InlineData(-6, -10, Direction.West, -16, 4, Direction.North)]
        [InlineData(11, 0, Direction.North, -13, 12, Direction.East)]
        [InlineData(-25, 9, Direction.North, 5, -22, Direction.West)]
        [InlineData(13, -20, Direction.East, -17, 11, Direction.South)]
        [InlineData(7, -5, Direction.West, -5, -19, Direction.East)]
        [InlineData(-24, -3, Direction.South, -1, 11, Direction.West)]
        [InlineData(-19, 11, Direction.East, 1, 12, Direction.South)]
        [InlineData(-8, -10, Direction.West, 4, -23, Direction.North)]
        [InlineData(14, 3, Direction.North, -25, -18, Direction.South)]
        [InlineData(-23, -6, Direction.East, -10, 14, Direction.East)]
        [InlineData(-22, 4, Direction.East, -5, 12, Direction.South)]
        [InlineData(-22, -7, Direction.North, 5, 12, Direction.South)]
        [InlineData(-17, -14, Direction.East, -21, 0, Direction.South)]
        [InlineData(8, 13, Direction.North, -3, -23, Direction.North)]
        [InlineData(12, -25, Direction.South, -11, 14, Direction.South)]
        [InlineData(6, 1, Direction.East, -2, -25, Direction.North)]
        [InlineData(-6, -24, Direction.North, -16, -25, Direction.West)]
        [InlineData(-19, -12, Direction.West, -1, -25, Direction.West)]
        [InlineData(6, -6, Direction.North, 13, 7, Direction.West)]
        [InlineData(-23, -20, Direction.West, 4, -22, Direction.North)]
        [InlineData(-25, -9, Direction.East, -12, -1, Direction.West)]
        [InlineData(2, 11, Direction.South, 9, -22, Direction.South)]
        [InlineData(8, -1, Direction.East, -4, -13, Direction.East)]
        [InlineData(-23, -22, Direction.South, -20, -10, Direction.West)]
        [InlineData(-6, -20, Direction.West, -25, -24, Direction.North)]
        [InlineData(-5, 1, Direction.South, 11, -9, Direction.North)]
        [InlineData(-25, -15, Direction.North, -25, 10, Direction.South)]
        [InlineData(-21, 6, Direction.North, 7, -12, Direction.South)]
        [InlineData(4, -17, Direction.South, -9, -11, Direction.North)]
        [InlineData(0, 11, Direction.West, -4, -7, Direction.West)]
        [InlineData(-16, -21, Direction.East, -14, 9, Direction.East)]
        [InlineData(10, -16, Direction.South, -23, 2, Direction.East)]
        [InlineData(13, -4, Direction.West, 12, -5, Direction.East)]
        [InlineData(-4, -19, Direction.North, -25, -18, Direction.South)]
        [InlineData(0, -14, Direction.West, -14, 8, Direction.East)]
        [InlineData(10, 8, Direction.North, -18, -14, Direction.South)]
        [InlineData(10, -9, Direction.West, -19, 3, Direction.North)]
        [InlineData(3, 2, Direction.South, 11, -5, Direction.West)]
        [InlineData(11, -13, Direction.North, -11, -18, Direction.East)]
        [InlineData(-4, 14, Direction.East, 0, -10, Direction.West)]
        [InlineData(14, 11, Direction.West, 0, -19, Direction.North)]
        [InlineData(-24, 8, Direction.West, -25, 10, Direction.West)]
        [InlineData(-17, -11, Direction.West, -25, 9, Direction.East)]
        [InlineData(-4, 2, Direction.East, 14, 0, Direction.North)]
        [InlineData(-9, 9, Direction.North, -23, 10, Direction.North)]
        [InlineData(-4, -6, Direction.South, -15, -9, Direction.North)]
        [InlineData(11, -2, Direction.West, -25, 9, Direction.West)]
        [InlineData(-1, -25, Direction.East, 7, 14, Direction.South)]
        [InlineData(-24, 13, Direction.West, 0, -7, Direction.North)]
        [InlineData(9, 2, Direction.West, -17, -21, Direction.South)]
        [InlineData(-24, 4, Direction.South, -15, 5, Direction.East)]
        [InlineData(-15, -13, Direction.East, -19, -12, Direction.South)]
        [InlineData(-19, 14, Direction.North, -16, -25, Direction.South)]
        [InlineData(-12, 7, Direction.East, -15, 12, Direction.South)]
        [InlineData(8, -22, Direction.West, 10, -20, Direction.East)]
        [InlineData(7, -2, Direction.West, -24, 7, Direction.East)]
        [InlineData(-8, 11, Direction.East, 14, -19, Direction.North)]
        [InlineData(3, -15, Direction.West, -22, -25, Direction.East)]
        [InlineData(-25, -25, Direction.North, 3, -15, Direction.South)]
        [InlineData(-7, -17, Direction.South, -22, 5, Direction.South)]
        [InlineData(0, -7, Direction.South, -11, -21, Direction.East)]
        [InlineData(-5, 12, Direction.South, 6, -17, Direction.South)]
        [InlineData(-17, 2, Direction.North, -1, 14, Direction.North)]
        [InlineData(6, -25, Direction.East, -1, -15, Direction.South)]
        [InlineData(-24, 9, Direction.South, -6, 14, Direction.West)]
        [InlineData(2, -11, Direction.South, -3, -25, Direction.East)]
        [InlineData(-19, -9, Direction.South, -7, -24, Direction.South)]
        [InlineData(-12, -5, Direction.West, 13, -19, Direction.North)]
        [InlineData(-11, 5, Direction.North, -22, 7, Direction.East)]
        [InlineData(-14, -24, Direction.West, 13, 6, Direction.West)]
        [InlineData(-9, -19, Direction.West, 4, -20, Direction.North)]
        [InlineData(-10, -7, Direction.North, -12, -24, Direction.West)]
        [InlineData(-1, 6, Direction.West, -14, -7, Direction.East)]
        [InlineData(2, 9, Direction.West, -13, 11, Direction.North)]
        [InlineData(2, -12, Direction.South, -10, 6, Direction.North)]
        [InlineData(13, 0, Direction.East, 12, 14, Direction.North)]
        [InlineData(-25, 1, Direction.West, 7, 11, Direction.South)]
        [InlineData(8, -21, Direction.East, -18, 0, Direction.South)]
        [InlineData(-3, -23, Direction.East, -10, -1, Direction.West)]
        [InlineData(-5, -21, Direction.North, 0, -19, Direction.West)]
        [InlineData(1, -5, Direction.North, -25, 5, Direction.South)]
        [InlineData(-11, -7, Direction.North, -8, 4, Direction.East)]


        public void Test3(
            int initialX,
            int initialY,
            Direction initialDirection,

            int expectedX,
            int expectedY,
            Direction expectedDirection
        )
        {
            // -- arrange
            var initialRover  = new Rover(initialX , initialY , initialDirection ); // 0,0, N
            var expectedRover = new Rover(expectedX, expectedY, expectedDirection);
            // -- act

            string instructions = initialRover.GetInstructions(expectedRover);

            initialRover.DoInsts(
                instructions
            );

            // -- assert
            initialRover.Cordinates.Should().BeEquivalentTo(expectedRover.Cordinates, "you should check smth");
            initialRover.Direction.Should().BeEquivalentTo(expectedDirection, o => o.ComparingEnumsByName(), "Instruction is " + instructions );
        }
    }
}
