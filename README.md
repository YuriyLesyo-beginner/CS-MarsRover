# Program`s description

The program is given a set of start coordinates and end coordinates. 
It should output instructions that Mars Rover needs to navigate from the start to the end.
The coordinates are provided in this format: (X,Y,Direction). 
The instructions are output in the "LLMMLR" format. 
You are expected to write a program that works for any set of coordinates. 

**Especially for:**
```
(0,0,N) (-2,3,E)
(-1,-1,W) (-2,-2,S)
(5,-3,N) (-10,13,S)
```
There are many paths that can be taken between two points on the map, and many of them have the same length. 
You should generate a path with a length that will be close (or the same) as the manhattan distance between the coordinates, but you don't have to think much about optimization. Your approach should not brute force the solution.
Use TDD to write your code. Make sure to show reasonable examples of your tests. And make sure both your code and your tests are readable. (Again, a good way to provide more meaning, is using them because of a parameter of fluent assertions.)
A finished application should consist of a test project that holds your tests, a core project that holds the application core logic, and a console application that allows for entering the start and end coordinates interactively, and prints the resulting list of instructions.