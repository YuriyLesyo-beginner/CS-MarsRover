﻿using System;
using MarsRover.Core;

namespace MarsRover.ConsoleApp
{
    class MarsRoverProgram
    {
        static Rover ReadAndCreateRover()
        {
            Console.WriteLine("Write x");
            var x = Console.ReadLine();
            Console.WriteLine("Write y");
            var y = Console.ReadLine();
            Console.WriteLine("Write directions");
            string dir = Console.ReadLine();
            return new Rover(Convert.ToInt32(x), Convert.ToInt32(y), GetDirection(dir));
        }
        static Direction GetDirection(string dir)
        {
            switch(dir)
            {
                case "W":
                    return Direction.West;
                case "N":
                    return Direction.North;
                case "E":
                    return Direction.East;
                case "S":
                    return Direction.South;
                default:
                    throw new InvalidOperationException(
                        $"You should write W, N, E,S only. You wrote -  '{dir}'"
                       );
            
            }
        }

        static void Main(string[] args)
        {
            while(true)
            {
                Console.WriteLine("Write start`s rover");
                Rover StartRover = ReadAndCreateRover();
                Console.WriteLine("Write end`s rover");
                Rover EndRover   = ReadAndCreateRover();

                Console.WriteLine(
                    $"The list of instructions is = '{StartRover.GetInstructions(EndRover)}'"
                );
            }
       

         }
    }
}
