﻿sing System;
namespace MarsRover.Core
{
    public enum Direction
    {
        North,
        East,
        South,
        West
    }
}
