﻿using System;
namespace MarsRover.Core
{
    /** Class for coordinates representing 
        C_x - x cords;
        C_y - y cords;
    */
    public class Cordinates
    {
        public int X { get; set; }
        public int Y { get; set; }

        /** Constructor
            If has empty parameters, create a object with coordinates (0,0)
            else create a object with the parameters @param p_x, @param p_y
        */
        public Cordinates(int x = 0, int y = 0)
        {
            this.X = x;
            this.Y = y;
        }
    }
}
