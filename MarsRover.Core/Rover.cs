﻿using System;


namespace MarsRover.Core
{
    /** Represent Rover's class 
        R_Cordinates = rover's cordinates (x,y)
        Direction  = rover'a direciton  (West, North, East, South)

        METHODS:
        doInsts         {void}  - change dirrection (in case of Left, Right 
                                  instructions) or corditates (Move istruction).
                                  @param {Instr[]} arrOfInstrs or 
                                  @param {String}  instrs - list of instructions to do
        getLessInsts    {void}  - get and change @param {ref string} instrs
                                  for getting less list of instructions (LLL = R ...)

        Move            {void}  - change cordinates according to direction
    */
    public class Rover
    {
        public Cordinates Cordinates { get; set; }   // coordinates of our Rover (x,y)
        public Direction Direction { get; set; }       // Rover`s direction

        /** Constructor
            In case of empty parameters, create a object with coordinates (0,0)
                and direction "North"
            In other case, create a object with the parameters @param p_x, @param p_y
                and @param p_dirz
        */
        public Rover(int x = 0,
                     int y = 0,
                     Direction dir = Direction.North)
        {
            this.Cordinates = new Cordinates(x, y);
            this.Direction = dir;
        }

        /** Set directions or cordinates according to instructions

            @params arrOfInstrs - array with Instrutions to do
        */
        public void DoInsts(params Instruction[] arrOfInstrs)
        {
            foreach (var instruction in arrOfInstrs)
            {
                switch (instruction)
                {
                    case Instruction.Left:
                        this.Direction--;      // turn Left
                        break;
                    case Instruction.Rigth:
                        this.Direction++;      // turn Rigth
                        break;
                    case Instruction.Move:
                        this.Move();
                        break;
                    default:

                        break;
                }

                // set right direction value
                // after calculus on switch, we can get -1 (in case of instruction Left)
                // or we can get 4 (Right)
                if ((int)Direction < 0 || (int)Direction > 3)
                    this.Direction = (Direction)(
                        ((int)(Direction) + 4) % 4
                    );

            }
        }


        /** Set directions or cordinates according to instructions

            @params instrs - string with Instrutions to do
        */
        public void DoInsts(string instrs)
        {

            foreach (var instruction in instrs)
            {
                switch (instruction)
                {
                    case 'L':
                        this.Direction--;      // turn Left
                        break;
                    case 'R':
                        this.Direction++;      // turn Rigth
                        break;
                    case 'M':
                        this.Move();
                        break;
                    default:
                        throw new InvalidOperationException(
                            $"Unknown instruction '{instruction}'"
                        );
                }

                // set right direction value
                // after calculus on switch, we can get -1 (in case of instruction Left)
                // or we can get 4 (Right)
                if ((int)Direction < 0 || (int)Direction > 3)
                    this.Direction = (Direction)(
                        ((int)(Direction) + 4) % 4
                    );

            }
        }



        /** Change coordinates according to directionxXXXX
               N
             W + E
               S
        */
        private void Move()
        {
            switch (Direction)
            {
                case Direction.North:
                    this.Cordinates.Y++;
                    break;
                case Direction.South:
                    this.Cordinates.Y--;
                    break;
                case Direction.East:
                    this.Cordinates.X++;
                    break;
                case Direction.West:
                    this.Cordinates.X--;
                    break;
            }
        }


        public string GetInstructions(Rover NewRover)
        {
            return new GetInstructions().To(this, NewRover);
        }
    }
}




  