﻿using System;
namespace MarsRover.Core
{
    /** Represent a list of direction's options
           N
         W + E
           S
     */
    public enum Direction
    {
        North,
        East,
        South,
        West
    }
}
