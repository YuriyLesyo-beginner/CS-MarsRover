﻿using System;
namespace MarsRover.Core
{
    // helping class to find a list of instructions for going from rover`s Start position
    /// to rover`s end position 
    public class GetInstructions
    {
        string Instructions;
        Direction ActualDirecton;

        public GetInstructions()
        {
            this.Instructions = "";
            this.ActualDirecton = Direction.North;
        }

       
        public string To(Rover StartRover,
                         Rover EndRover)
        {
            this.ActualDirecton = StartRover.Direction;

            // GETTING INSTRUCTIONS FOR Y
            GetInstructionsForCordinate(CordinatesOf.Y, EndRover.Cordinates.Y - StartRover.Cordinates.Y);
            // GETTING INSTRUCTIONS FOR X
            GetInstructionsForCordinate(CordinatesOf.X, EndRover.Cordinates.X - StartRover.Cordinates.X);
            // GETTING INSTRUCTIONS FOR DIRECTION
            if (this.ActualDirecton != EndRover.Direction)
                GetInstructionsForDirection(EndRover.Direction);

            return Instructions;
        }


        private void GetInstructionsForDirection(Direction TargetDirection)
        {
            var qtyOfCommands = this.ActualDirecton - TargetDirection;
            var AbsOfQty      = Math.Abs(qtyOfCommands);
            this.ActualDirecton = TargetDirection;
            Instructions += AbsOfQty == 3
                            ? (qtyOfCommands < 0 ? "L" : "R")  // LLL = R || RRR = L
                            : new String(
                                qtyOfCommands < 0 ? 'R' : 'L',
                                AbsOfQty
                            );
        }

        private void GetInstructionsForCordinate(CordinatesOf Cordinate, int differenceBtwCordinates)
        {
            Direction TargetDirection;
            switch(Cordinate)
            {
                case(CordinatesOf.Y):
                    TargetDirection =
                    differenceBtwCordinates > 0 ? Direction.North : Direction.South;
                    break;

                case(CordinatesOf.X):
                    TargetDirection =
                    differenceBtwCordinates > 0 ? Direction.East : Direction.West;
                    break;
                default:
                    throw new InvalidOperationException(
                        $"New Type Of Cordinates? -  '{Cordinate}'"
                       );
            }
            if (this.ActualDirecton != TargetDirection)
                GetInstructionsForDirection(TargetDirection);

            Instructions += new String('M', Math.Abs(differenceBtwCordinates));
        }

        private enum CordinatesOf{X, Y}
    }
}
