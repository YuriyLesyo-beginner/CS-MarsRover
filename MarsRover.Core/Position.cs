﻿using System;
namespace MarsRover.Core
{
    public class Position
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Direction Direction { get; set; }

        public Position()
        {
            this.X = 0;
            this.Y = 0;
            this.Direction = Direction.North;
        }
        public Position(int x, int y, Direction d)
        {
            this.X = x;
            this.Y = y;
            this.Direction = d;
        }
    }
}
